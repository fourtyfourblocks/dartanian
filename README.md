# dartanian
Simple implementation of express like server framework.

## getting started
1.  Add the following to your **pubspec.yaml** and run **pub upgrade**

    ```yaml
        dependencies:
          dartanian:
            git:
                url: git@bitbucket.org:fourtyfourblocks/dartanian.git
                ref: master
            version: '>=0.0.9 <1.0.0'

    ```

2.  Add dartanian to some code and run it

    ```dart
        import 'package:dartanian/server.dart' as server;

        class Person {
            var name;
            Person(this.name);

            String toString() => 'Person {$name}';
            Map toJson() => {"name": name};
        }

        var app = new server.Server();
        app
            ..use(server.router())
            ..use(server.statics('/web'))
            ..use(server.notFound())
            ..setEnvAll({})
            ..setEnv(server.Env.VIEW_ENGINE, server.json)
            ..get('/ping', server.PingHandler)
            ..get('/{a}/{b}/toe/{c}', (req, res) => res.sendJson({
                                                                       "a": req.pathParam('a'),
                                                                       "b": req.pathParam('b'),
                                                                       "c": req.pathParam('c'),
                                                                       "d": req.queryParam('d')
                                                                   }))
            ..get('/person', (req, res) {
                res.sendJson(new Person(req.queryParam('name')));
            })
            ..listen(host, port);
        }
    ```

3.  Implemented features (so far)
    +   router handles GET, POST, PUT, etc
    +   router can lookup controllers using annotations
    +   server can handle static files - by default files are loaded from app's /web folder
    +   server can render data by two build in renderers - text and json
    +   server can handle 404 and returns not found message
    +   server handles all errors and return 500 error message
    +   errors are rendered using configured view_engine or text renderer
    +   simple concurrent util is available by `import 'package:dartanian/concurrent.dart'`

4.  Features to be implemented
    + add mustache and jade view engine
    + create TestHttpServer
    + create simple injection framework

5.  Examples
    +   **lookup controllers by annotations**
        1.  add Controller in separate dart file. for example in `lib/src/controller/ShoppingController.dart`
            ```dart
                part of my_app;

                @server.Path("/shopping")
                @server.Produces("application/json")
                class ShoppingController {
                    static final _logger = LoggerFactory.getLoggerFor(ShoppingController);

                    @server.GET
                    @server.Path("/product/{productId}")
                    @server.Consumes("application/json;charset=utf-8")
                    void buy(request, response) {
                        response.sendJson({
                                              "id": 1,
                                              "name": "Done buying $productId"
                                          });
                    }

                    @server.ANY
                    @server.Path("/throw/exception")
                    @server.Consumes("application/json;charset=utf-8")
                    void error(request, response) {
                        throw new Exception("uoops");
                    }
                }
            ```

        2.  configure your server

            ```dart
                library my_app;

                import 'package:dartanian/server.dart' as server;

                part "src/controllers/ShoppingController.dart";

                var app = new server.Server();
                app
                    ..use(server.router())
                    ..use(server.notFound())
                    ..get('/ping', server.PingHandler)
                    ..listen(host, port);
            ```

            now, watch your log to find which handlers where found during startup.
    +   **distribute work using isolates**
        1.  dart uses isolates to run code in parallel, you can spawn many isolates and distribute work between them. Isolates do not share any state and uses messages to pass information.

            ```dart
                library my_app;

                import 'package:dartanian/concurrent.dart' as concurrent;

                ...
                void buy(request, response) {
                    var qName = request.pathParam('name');
                    var c = new concurrent.Concurrent();
                    c.spawn(Worker.work, [{"name": qName}])
                    .receive().first
                    .whenComplete(() => c.stop([]))
                    .then((msg) {
                        print("Got message from worker $msg");
                        response.sendJson({
                                              "id": 1,
                                              "name": msg['name']
                                          });
                    });
                }

                class Worker {
                    static work(concurrent.Message<Map> msg) {
                        print("Got $msg.");
                        var name = msg.message['name'];
                        print("worker got ${name}.");
                        msg.pid.send({"name": "worker $name"});
                    }
                }
            ```
            If you want send messages back and forth you should create separate instance of Concurrent in Worker work.

# enjoy ! any feedback welcome !