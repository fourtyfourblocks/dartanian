part of dartanian;

abstract class Renderer {
    ContentType get contentType;

    Stream<List<int>> render(String template, dynamic model);
}
