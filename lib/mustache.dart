part of dartanian;

class mustache {
    mustache() {
    }
}


var source = '{{#names}}<div>{{lastname}}, {{firstname}}</div>{{/names}}';
var template = mustache.parse(source);
var model = {
    'names': [
        {
            'firstname': 'Greg', 'lastname': 'Lowe'
        },
        {
            'firstname': 'Bob', 'lastname': 'Johnson'
        }
    ]
};