library concurrent;

import "dart:async";
import "dart:core";
import "dart:isolate";
import "package:log4dart/log4dart_vm.dart";
import "package:dartanian/server.dart" as server show IllegalStateException;

class Message<T> {
    final SendPort _pid;
    final T _message;

    Message(this._pid, this._message);

    T get message => _message;

    SendPort get pid => _pid;

    String toString() => "Message {$_pid, $_message}.";
}

class Stop {
    const Stop();

    String toString() => "Stop.";
}

class Concurrent {
    static final _logger = LoggerFactory.getLoggerFor(Concurrent);

    ReceivePort _self;

    Stream receive() {
        _logger.debug("subscribing new receiver");
        if (_self == null) {
            _self = new ReceivePort();
        }

        return _self;
    }

    /**
     * spawns new process and passes message to it. message is wrapped in Message object.
     * message can be of any simple type, map and list and Object containing any of those.
     *
     * returns Stream to receive messages from isolate.
     */

    Concurrent spawn(Function workerName, List<Object> messages) {
        if (_self == null) {
            _self = new ReceivePort();
        }

        messages.forEach((message) {
            Isolate.spawn(workerName, new Message(_self.sendPort, message));
        });

        return this;
    }

    void tell(SendPort pid, message) {
        pid.send(new Message(_self.sendPort, message));
    }

    Future<Message> ask(SendPort pid, message) {
        var port = new ReceivePort();
        pid.send(new Message(port.sendPort, message));
        return port.first.whenComplete(port.close);
    }

    /**
     * you must call stop to correctly dispose resources.
     */

    void stop(List<SendPort> pids) {
        pids.forEach((pid) {
            pid.send(const Stop());
        });
        _self.close();
    }
}