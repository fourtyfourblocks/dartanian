library dartanian;

import "dart:async";
import "dart:io";
import "dart:convert";
import "dart:collection";
import "dart:mirrors";
import "package:log4dart/log4dart_vm.dart";

part 'renderer.dart';
part "src/method.dart";
part "src/resource.dart";
part "src/context.dart";
part 'src/defaultHandler.dart';
part 'src/defaultRenderer.dart';
part 'src/rsAnnotation.dart';
part 'src/rsServer.dart';

/*
 * TODO add mustache renderer
 * TODO implement reading data from request
 * TODO check other method handling
 * TODO create TestHttpServer with request and response mocks
 * TODO prepare unit test
 * TODO add jade renderer
 */

class Env {
    static final String VIEW_ENGINE = "view engine";
}

class Message {
    final int statusCode;
    final Object error;
    final String message;
    final String statusReason;

    Message({this.statusCode: 200, this.error, this.message, this.statusReason});

    String toString() => "Server response : status code = $statusCode, message = $message, reason = $statusReason, error = $error";

    Map toJson() => {
        "statusCode": statusCode, "error": error != null ? error.toString() :
                                           null, "message": message, "statusReason": statusReason
    };
}
/**
 * Http Method object.
 */
abstract class Method {
    String get name;
}

// The signature your Request Handlers should implement
typedef void RequestHandler (Request request, Response response);

typedef void Hook (Request request);

/**
 * Responses to client with pong message, use it for heart beat services.
 */

void PingHandler(_, Response response) => response.sendText("pong");

/**
 * Abstract unit capable of handling a resource
 */
abstract class ResourceHandler {
    ResourceHandler findHandler(HttpRequest request);

    Future handle(Request request, Response response, _ExecutionContext context);
}

abstract class ErrorHandler {
    Future onError(Exception exception, Request request, Response response, _ExecutionContext context);
}

/**
 *  The server API to configure your app with.
 */
abstract class Server {
    factory Server() = _RestServer;

    //Register a request handler that will be called for a matching GET request

    void get(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that will be called for a matching POST request

    void post(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that will be called for a matching PUT request

    void put(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that will be called for a matching DELETE request

    void delete(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that will be called for a matching PATCH request

    void patch(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that will be called for a matching HEAD request

    void head(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that will be called for a matching OPTIONS request

    void options(String path, RequestHandler handler, [ContentType type]);

    //Register a request handler that handles ANY verb

    void any(String path, RequestHandler handler, [ContentType type]);

    // Register a global handler such like static or mustache.

    void use(ResourceHandler handler);

    //Add any global properties

    void setEnv(String key, String value);

    //Add any global properties

    void setEnvAll(Map<String, Object> properties);

    void addPreHook(Hook hook);

    void addPostHook(Hook hook);

    // When all routes and modules are registered - Start the HttpServer on host:port
    Future<HttpServer> listen([String host, int port]);
}

/**
 * An encapsulation for HttpRequest and HttpResponse objects
 * with useful overloads for each for common operations and usage patterns.
 */
abstract class Request {
    //Context
    HttpRequest get httpRequest;

    bool get hasHandler;

    String get path;
    String pathParam(String key);
    String queryParam(String key);
    Cookie cookie(String key);

    String header(String key);

    //Read APIs
    ContentType get contentType;
    Future<Stream<List<int>>> readAsBytes();
    Future<String> readAsText([Encoding encoding]);
    Future<Object> readAsJson([Encoding encoding]);
    Future<Object> readAsObject([Encoding encoding]);
}

abstract class Response {
    HttpResponse get httpResponse;

    Future get responseFuture;

    Future get completeFuture;

    bool get handled;

    //Write APIs

    void contentType(ContentType contentType);

    void statusCode(int code);

    void statusReason(String reason);

    void cookie(String key, String value);

    void header(String key, String value);

    void send([Object value, String statusReason]);
    void sendJson(Object value);
    void sendHtml(Object value);
    void sendText(Object value);
    void sendBytes(Stream<List<int>> bytes);

    /**
     *
     * redirects client to other location.
     *
     * toPath - redirect to given path
     * permanently - whether redirect is permanent or not
     */
    void redirect(String toPath, [bool permanently = false]);

    /**
     * Use template renderer to create response message.
     *
     * templatePath to template as rendering engine understands it.
     * viewModel an optional model with data.
     */
    void render(String templatePath, [dynamic viewModel]);

    void giveUp();
}

class Mime {
    static final ContentType APPLICATION_JSON = new ContentType("application", "json", charset: "utf-8");
    static final ContentType TEXT_PLAIN = new ContentType("text", "plain", charset: "utf-8");
    static final ContentType TEXT_HTML = new ContentType("text", "html", charset: "utf-8");
    static final ContentType APPLICATION_FORM_URL_ENCODED = new ContentType("application", "x-www-form-urlencoded",
                                                                            charset: "utf-8");
    static final ContentType MULTIPART_FORM_DATA = new ContentType("multipart", "form-data");
}

class ResourceAlreadyDefinedException implements Exception {
    String resourceName;
    ResourceAlreadyDefinedException(this.resourceName);
    String toString() => "$resourceName is already defined.";
}

class ResourceNotHandledException implements Exception {
    String resourceName;

    ResourceNotHandledException(this.resourceName);

    String toString() => "$resourceName was not handled.";
}

class IllegalStateException implements Exception {
    String reason;

    IllegalStateException(this.reason);

    String toString() => "Illegal state detected. $reason.";
}

void noop([_]) {
}

void notImplemented([_]) => throw new Exception("not implemented");

// define global application properties

String get appDir => Directory.current.parent.path;