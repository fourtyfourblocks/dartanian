part of dartanian;

class _Method implements Method {
    final String _name;

    const _Method(this._name);

    String get name => _name;

    String toString() => '$_name';
}

const GET = const _Method("GET");
const POST = const _Method("POST");
const PUT = const _Method("PUT");
const DELETE = const _Method("DELETE");
const PATCH = const _Method("PATCH");
const HEAD = const _Method("HEAD");
const OPTIONS = const _Method("OPTIONS");
const ANY = const _Method("ANY");
