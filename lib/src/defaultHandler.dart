part of dartanian;

// define handler aliases

ResourceHandler router() => new _UriResourceHandler();

ResourceHandler statics([String pathToPublic]) => new _StaticFiles(pathToPublic);

ResourceHandler notFound() => new _ResourceNotFoundHandler();

class _UriResourceHandler implements ResourceHandler {
    static final _logger = LoggerFactory.getLoggerFor(_UriResourceHandler);

    Map<String, List<_UriResource>> _handlers = new HashMap<String, List<_UriResource>>();

    void assignAction(action, path, handler, ContentType type) {
        var requestHandlers = _getActionCollection(_handlers, action.name);
        _checkPath(requestHandlers, path);
        var resource = new _UriResource(action, path, handler, type);
        _logger.info("adding resource $resource.");
        requestHandlers.add(resource);
    }

    @override
    ResourceHandler findHandler(HttpRequest request) {
        var action = request.method.toUpperCase();
        var handler = _findHandlerByAction(request, action);
        if (handler == null) {
            handler = _findHandlerByAction(request, ANY.name);

        }
        return handler;
    }

    _findHandlerByAction(request, action) {
        return _getActionCollection(_handlers, action).firstWhere((r) => r.canHandle(request),
                                                                  orElse: () => null);
    }

    @override
    Future handle(Request request, Response response, _ExecutionContext context) {
        throw new IllegalStateException("This is proxy handler delegating work to registerd uri handlers");
    }

    /**
     * traverse app context and find additional handlers
     */

    Future prepare() {
        return new Future.sync(() {
            return _mergeResources(_searchUriResources(), _handlers);
        });
    }

    Map _searchUriResources() {
        Map<String, List<_UriResource>> scannedHandlers = {
        };

        var mirrors = currentMirrorSystem();
        mirrors.libraries.forEach((key, LibraryMirror lib) {
            lib.declarations.forEach((_, classMirror) {
                if (classMirror is ClassMirror) {
                    var classMeta = {
                    };
                    classMirror.metadata.forEach((meta) {
                        if (meta.hasReflectee) {
                            var reflectee = meta.reflectee;
                            if (reflectee is Path)
                                classMeta['path'] = reflectee.path;
                            if (reflectee is Consumes)
                                classMeta['consumes'] = reflectee.contentType;
                            if (reflectee is Produces)
                                classMeta['produces'] = reflectee.contentType;
                        }
                    });
                    if (classMeta['path'] == null) {
                        return;
                    }
                    var instance = classMirror.newInstance(new Symbol(''), []);
                    classMirror.declarations.forEach((_, methodMirror) {
                        if (methodMirror is MethodMirror && methodMirror.isRegularMethod) {
                            var methodRef = methodMirror.simpleName;
                            var path = classMeta['path'];
                            var method;
                            var consumes;
                            var produces;
                            methodMirror.metadata.forEach((meta) {
                                if (meta.hasReflectee) {
                                    var reflectee = meta.reflectee;
                                    if (reflectee is Method) {
                                        method = reflectee;
                                    }
                                    if (reflectee is Path) {
                                        path += reflectee.path;
                                        path = path.replaceAll(r"//", "/");
                                    }
                                    if (reflectee is Consumes)
                                        consumes = reflectee.contentType;
                                    if (reflectee is Produces)
                                        produces = reflectee.contentType;
                                }
                            });
                            if (method == null) {
                                throw new IllegalStateException("Controller method [$methodRef] must declare method name (GET, POST, PUT, etc ..)");
                            }

                            var handler = (request, response) => instance.invoke(methodRef, [request, response]);
                            var resource = new _UriResource(method, path, handler,
                                                            _getCT(classMeta['produces'], produces),
                                                            _getCT(classMeta['consumes'], consumes));
                            _logger.info("adding resource $resource.");

                            var handlers = _getActionCollection(scannedHandlers, method.name);
                            handlers.add(resource);
                        }
                    });
                }
            });
        });

        return scannedHandlers;
    }

    ContentType _getCT(classType, methodType) {
        var result;
        var type = (methodType == null) ? classType : methodType;
        if (type != null) {
            result = ContentType.parse(type);
        }

        return result;
    }

    Map<String, List<_UriResource>> _mergeResources(endpoints, handlers) {
        endpoints.forEach((action, handlersList) {
            var masterHandlers = _getActionCollection(handlers, action);
            handlersList.forEach((_UriResource handlerToAdd) {
                _checkPath(masterHandlers, handlerToAdd.path);
                masterHandlers.add(handlerToAdd);
            });
        });
        return handlers;
    }

    _getActionCollection(handlers, action) {
        if (!handlers.containsKey(action)) {
            handlers[action] = [];
        }

        return handlers[action];
    }

    void _checkPath(handlers, path) {
        var resource = handlers.firstWhere((resource) => resource.path == path,
                                           orElse: () => null);
        if (resource != null)
            throw new ResourceAlreadyDefinedException(path);
    }

}

class _StaticFiles implements ResourceHandler {
    static final _logger = LoggerFactory.getLoggerFor(_StaticFiles);

    String _root;
    String _defaultFile = 'index.html';

    _StaticFiles([this._root]) {
        _root = _root == null ? '$appDir/web' : _root;
        _logger.info('will look up static files in $_root directory.');
    }

    String _buildPath(String path) {
        var sb = new StringBuffer(_root);
        sb.write(path);

        if (!path.contains('.')) {
            if (!path.endsWith('/')) {
                sb.write('/');
            }
            sb.write(_defaultFile);
        }

        return sb.toString();
    }

    ContentType _contentTypeBy(pathToFile) {
        var extension = pathToFile.split('.').last;
        ContentType ct = _contentTypeByExtensions[extension];
        if (ct == null) {
            ct = _contentTypeByExtensions['txt'];
        }
        return ct;
    }

    @override
    ResourceHandler findHandler(HttpRequest request) => this;

    @override
    Future handle(Request request, Response response, _ExecutionContext context) {
        var pathToFile = _buildPath(request.path);

        _logger.debug("trying to lookup $pathToFile.");
        var completer = new Completer();

        File file = new File(pathToFile);
        file
        .exists()
        .then((bool exists) {
            if (exists) {
                response.statusCode(200);
                response.contentType(_contentTypeBy(pathToFile));
                response.sendBytes(file.openRead());
            }
            else {
                _logger.debug("requested file ${request.path} doesn't exeists. Giving up.");
                response.giveUp();
            }

            completer.complete(response);
        });

        return completer.future;
    }
}

class _ResourceNotFoundHandler implements ResourceHandler {
    static final _logger = LoggerFactory.getLoggerFor(_ResourceNotFoundHandler);

    ResourceHandler findHandler(HttpRequest request) {
        return this;
    }

    Future handle(Request request, Response response, _ExecutionContext context) {
        var message = new Message(
            statusCode: 404,
            message: "A handler for ${request.path} does not exists.",
            statusReason: "No resource found for ${request.path}."
            );
        response.statusCode(message.statusCode);
        response.statusReason(message.statusReason);
        response.render(null, message);

        return new Future.value(response);
    }
}

class _ErrorHandler implements ErrorHandler {
    static final _logger = LoggerFactory.getLoggerFor(_ErrorHandler);

    Future onError(exception, Request request, Response response, _ExecutionContext context) {
        var message = new Message(
            statusCode: 500,
            error: exception,
            message: "An error occuerd while handling ${request.path}",
            statusReason: "Error occured while handling request to ${request.path} with $exception"
            );
        response.statusCode(message.statusCode);
        response.statusReason(message.statusReason);
        response.render(null, message);

        return response.responseFuture;
    }
}

final Map<String, ContentType> _contentTypeByExtensions = {
    'txt': new ContentType("text", "plain", charset: "utf-8"),
    'htm': new ContentType("text", "html", charset: "utf-8"),
    'html': new ContentType("text", "html", charset: "utf-8"),
    'js': new ContentType("application", "javascript", charset: "utf-8"),
    'css': new ContentType("text", "css", charset: "utf-8"),
    'gz': new ContentType("application", "x-gzip"),
    'zip': new ContentType("application", "zip"),
    '7z': new ContentType("application", "x-7z-compressed"),
    'tar': new ContentType("application", "x-tar"),
    'swf': new ContentType("application", "x-shockwave-flash"),
    'pdf': new ContentType("application", "pdf"),
    'json': new ContentType("application", "json", charset: "utf-8"),
    'csv': new ContentType("text", "csv"),
    'xml': new ContentType("application", "xml", charset: "utf-8"),
    'bmp': new ContentType("image", "bmp"),
    'gif': new ContentType("image", "gif"),
    'ico': new ContentType("image", "x-icon"),
    'jpe': new ContentType("image", "jpeg"),
    'jpeg': new ContentType("image", "jpeg"),
    'jpg': new ContentType("image", "jpeg"),
    'png': new ContentType("image", "png"),
    'svg': new ContentType("image", "svg+xml"),
};