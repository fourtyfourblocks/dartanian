part of dartanian;

class _RestServer implements Server {
    static final _logger = LoggerFactory.getLoggerFor(_RestServer);

    final List<ResourceHandler> _resourceHandlers = [];
    final List<ErrorHandler> _errorHandlers = [];
    final List<Hook> _preHooks = [];
    final List<Hook> _postHooks = [];
    final Map<String, Object> _env = {
    };

    _UriResourceHandler _uriResourceHandler;

    void get(String path, RequestHandler handler, [ContentType type]) => _assignAction(GET, path, handler, type);

    void post(String path, RequestHandler handler, [ContentType type]) => _assignAction(POST, path, handler, type);

    void put(String path, RequestHandler handler, [ContentType type]) => _assignAction(PUT, path, handler, type);

    void delete(String path, RequestHandler handler, [ContentType type]) => _assignAction(DELETE, path, handler, type);

    void patch(String path, RequestHandler handler, [ContentType type]) => _assignAction(PATCH, path, handler, type);

    void head(String path, RequestHandler handler, [ContentType type]) => _assignAction(HEAD, path, handler, type);

    void options(String path, RequestHandler handler, [ContentType type]) => _assignAction(OPTIONS, path, handler,
                                                                                           type);

    void any(String path, RequestHandler handler, [ContentType type]) => _assignAction(ANY, path, handler, type);

    //Setup app

    void use(ResourceHandler handler) {
        if (handler is ErrorHandler) {
            _errorHandlers.add(handler);
        }
        else {
            if (handler is _UriResourceHandler && _uriResourceHandler == null) {
                _uriResourceHandler = handler;
            }
            _resourceHandlers.add(handler);
        }
    }

    //Add any global properties

    void setEnv(String key, Object value) {
        _env[key] = value;
    }

    Object getEnv(String key, [Object def]) {
        var e = _env[key];
        return (e == null) ? def : e;
    }

    //Add any global properties

    void setEnvAll(Map<String, Object> properties) {
        properties.forEach((key, value) {
            _logger.info("apps args : $key=$value");
            setEnv(key, value);
        });
    }

    void addPreHook(Hook hook) {
        _preHooks.add(hook);
    }

    void addPostHook(Hook hook) {
        _postHooks.add(hook);
    }

    void _assignAction(action, path, handler, type) {
        if (_uriResourceHandler == null) {
            _logger.warn("There is no uri router installed. Check your app configuration.");
            _logger.warn("Make sure to add app..use(router());.");
            throw new IllegalStateException("There is no uri router installed. Check your app configuration.");
        }
        _uriResourceHandler.assignAction(action, path, handler, type);
    }

    // When all routes and modules are registered - Start the HttpServer on host:port

    Future<HttpServer> listen([String host, int port]) {
        if (_resourceHandlers.isEmpty) {
            _logger.warn("There are no resource handlers installed.");
            _logger.warn("Use at least one of : router, statics, notFound, etc");
        }

        return _uriResourceHandler.prepare()
        .then((Map<String, List<_UriResource>> endpoints) {
            var serverFtr = HttpServer.bind(host, port);
            serverFtr.then((server) {
                _logger.info("Server listening on $host:$port...");
                server.listen(_dispatch(_resourceHandlers));
            });

            return serverFtr;
        });
    }

    _dispatch(handlers) {
        if (_errorHandlers.isEmpty) {
            _errorHandlers.add(new _ErrorHandler());
        }
        var executionContext = new _DefaultExecutionContext(this);
        return (HttpRequest request) {
            _execute(request, handlers, executionContext);
        };
    }

    void _execute(request, handlers, ex) {
        var sw = new Stopwatch()
            ..start();
        var req = new _RequestWrapper(request, null);
        var res = new _ResponseWrapper(request.response, ex);

        _executeHooks(request, _preHooks)
        .then((request) {
            return new Future.sync(() {
                var handlerIterator = (resource) {
                    if (!res.handled) {
                        var handler = resource.findHandler(request);
                        if (handler != null) {
                            req = new _RequestWrapper(request, handler is _UriResource ? handler : null);
                            return handler.handle(req, res, ex);
                        }
                    }

                    return new Future.value(res);
                };
                return _firstWhere(handlers, handlerIterator, (res) => res.handled).then((response) {
                    if (!res.handled) {
                        throw new ResourceNotHandledException("${req.path}");
                    }

                    return res.responseFuture;
                });
            });
        })
        .then((_) => _executeHooks(request, _postHooks))
        .catchError((e, stack) {
            _logger.error("Handling ${req.path} finished with an error: $e");
            _logger.debug(stack.toString());
            return _errorHandlers[0].onError(e, req, res, ex);
        })
        .whenComplete(() {
            request.response
            .close()
            .then((resp) => _logger.info("Closed request to ${request.uri.path} with status ${resp.statusCode}."));
            sw.stop();
            _logger.info("Call to ${request.method} ${request.uri} ended in ${sw.elapsedMilliseconds} ms");
        });
    }

    Future _firstWhere(Iterable input, Future f(element), bool e(value)) {
        Completer doneSignal = new Completer();
        Iterator iterator = input.iterator;

        void nextElement(value) {
            if (value != null && e(value)) {
                doneSignal.complete(value);
                return;
            }
            if (iterator.moveNext()) {
                new Future.sync(() => f(iterator.current)).then(nextElement, onError: doneSignal.completeError);
            }
            else {
                doneSignal.complete(null);
            }
        }
        nextElement(null);
        return doneSignal.future;
    }

    Future _executeHooks(request, List<Hook> hooks) {
        return new Future.sync(() {
            hooks.forEach((hook) => hook(request));
            return request;
        });
    }
}