part of dartanian;

class Path {
    final String path;

    const Path(this.path);

    String toString() => '$path';
}

class Consumes {
    final String contentType;

    const Consumes(this.contentType);

    String toString() => '$contentType';
}

class Produces {
    final String contentType;

    const Produces(this.contentType);

    String toString() => '$contentType';
}

