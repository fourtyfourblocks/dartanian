part of dartanian;

abstract class _ExecutionContext {
    Future send(Response response, [Object value, String statusReason]);

    Future sendJson(Response response, Object value);

    Future sendHtml(Response response, Object value);

    Future sendText(Response response, Object value);

    Future sendBytes(Response response, Stream<List<int>> inputStream);

    Future render(Response response, String template, [dynamic model]);

    Future<Response> waitFor(Response response);
}

class _DefaultExecutionContext implements _ExecutionContext {
    static final _logger = LoggerFactory.getLoggerFor(_DefaultExecutionContext);

    final _RestServer _server;
    Renderer _defaultRenderer;

    _DefaultExecutionContext(this._server) {
        this._defaultRenderer = _server.getEnv('view engine', text) as Renderer;
    }

    Renderer get defaultRenderer => _defaultRenderer;

    @override
    Future sendBytes(Response response, Stream<List<int>> inputStream) {
        return response.httpResponse
        .addStream(inputStream)
        .then((_) => response.httpResponse.flush());
    }

    @override
    Future send(Response response, [Object value, String statusReason]) {
        response.statusReason(statusReason);
        sendBytes(response, defaultRenderer.render(null, value));
    }

    @override
    Future sendJson(Response response, Object value) {
        response.contentType(Mime.APPLICATION_JSON);
        response.statusCode(200);
        return sendBytes(response, json.render(null, value));
    }

    @override
    Future sendHtml(Response response, Object value) {
        response.contentType(Mime.TEXT_HTML);
        response.statusCode(200);
        return sendBytes(response, text.render(null, value));
    }

    @override
    Future sendText(Response response, Object value) {
        response.contentType(Mime.TEXT_PLAIN);
        response.statusCode(200);
        return sendBytes(response, text.render(null, value));
    }

    @override
    Future render(Response response, String template, [dynamic model]) {
        response.contentType(defaultRenderer.contentType);
        return sendBytes(response, defaultRenderer.render(template, model));
    }

    Future<Response> waitFor(Response response) {
        return response.completeFuture.then((hasCompleted) => response);
    }
}

class _RequestWrapper implements Request {
    final HttpRequest _request;
    final _UriResource _resource;
    Map<String, String> _pathParams;

    _RequestWrapper(this._request, this._resource) {
        _pathParams = _resource != null ?
                      _resource.extractPathParams(_request) : {
        };

    }

    String get path => _request.uri.path;

    HttpRequest get httpRequest => _request;

    bool get hasHandler => _resource != null;

    ContentType get contentType {
        return _request.headers[HttpHeaders.CONTENT_TYPE] != null ?
               _request.headers[HttpHeaders.CONTENT_TYPE][0] :
               null;
    }

    String header(String key) {

    }

    String pathParam(String key) => _pathParams[key];

    String queryParam(String key) => _request.uri.queryParameters[key];

    Cookie cookie(String key) => _request.cookies.firstWhere((cookie) => cookie.name == key,
                                                             orElse: () => null);

    Future<Object> readAsObject([Encoding encoding]) {

    }

    Future<Object> readAsJson([Encoding encoding]) {

    }

    Future<String> readAsText([Encoding encoding]) {

    }

    Future<Stream<List<int>>> readAsBytes() {

    }
}

class _ResponseWrapper implements Response {
    final HttpResponse _response;
    final _ExecutionContext _execution;
    Completer _completer;

    Future _responseFuture;
    bool _handled = false;

    _ResponseWrapper(this._response, this._execution);

    HttpResponse get httpResponse => _response;

    bool get handled => _handled;

    void contentType(ContentType ct) {
        _response.headers.contentType = ct;
    }

    void cookie(String key, String value) {

    }

    void statusCode(int code) {
        _response.statusCode = code;
    }

    void statusReason(String reason) {
        _response.reasonPhrase = reason;
    }

    void header(String key, String value) {
        _response.headers.add(key, value);
    }

    @override
    void redirect(String toPath, [bool permanently = false]) {

    }

    @override
    void send([Object value, String statusReason]) {
        _finishResponse(_execution.send(this, value, statusReason));
    }

    @override
    void sendJson(Object value) => _finishResponse(_execution.sendJson(this, value));

    @override
    void sendHtml(Object value) => _finishResponse(_execution.sendHtml(this, value));

    @override
    void sendText(Object value) => _finishResponse(_execution.sendText(this, value));

    @override
    void sendBytes(Stream<List<int>> bytes) => _finishResponse(_execution.sendBytes(this, bytes));

    @override
    void render(String templatePath, [dynamic viewModel]) => _finishResponse(_execution.render(this, templatePath,
                                                                                               viewModel));

    void _finishResponse(Future f) {
        _responseFuture = f;
        _completeResponse(true);
    }

    void _completeResponse(bool hasCompleted) {
        _handled = hasCompleted;
        if (_completer != null)
            _completer.complete(hasCompleted);
    }

    Future get responseFuture => _responseFuture;

    Future get completeFuture {
        if (_completer == null) {
            _completer = new Completer();
        }

        return _completer.future;
    }

    void giveUp() {
        _completeResponse(false);
    }
}
