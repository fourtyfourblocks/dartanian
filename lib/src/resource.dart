part of dartanian;

class _UriResource implements ResourceHandler {
    static final _logger = LoggerFactory.getLoggerFor(_UriResource);

    String _path;
    Method _method;
    RequestHandler _handler;
    ContentType _consumes;
    ContentType _produces;

    String get path => _path;

    Method get method => _method;

    RequestHandler get handler => _handler;

    ContentType get consumes => _consumes;

    ContentType get produces => _produces;

    // path should match
    RegExp _pathPattern;
    List _pathParamNames = [];

    _UriResource(this._method, this._path, this._handler, [this._produces, this._consumes]) {
        var replacedString = this.path.replaceAllMapped(new RegExp(r"{(\w+?)}"), (Match m) {
            _pathParamNames.add(m.group(1));
            return r"(\w+)";
        });
        this._pathPattern = new RegExp(r'^' + replacedString + r'$');
    }

    bool canHandle(HttpRequest request) {
        var matches = _pathPattern.hasMatch(request.uri.path);
        if (matches) {
            _logger.debug("Matched ${request.method}:${request.uri.path} to ${this}");
        }
        return matches;
    }

    Map<String, String> extractPathParams(HttpRequest request) {
        var uriParams = {
        };
        if (_pathParamNames.isNotEmpty) {
            var match = _pathPattern.firstMatch(request.uri.path);
            for (var i = 0; i < match.groupCount; i++) {
                String group = match.group(i + 1);
                uriParams[_pathParamNames[i]] = group;
            }
        }

        return uriParams;
    }

    @override
    ResourceHandler findHandler(HttpRequest request) => this;

    @override
    Future handle(Request request, Response response, _ExecutionContext context) {
        handler(request, response);
        return context.waitFor(response);
    }

    String toString() => '$method: $path [consumes: $_consumes, produces: $_produces]';
}