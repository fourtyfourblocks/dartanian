part of dartanian;

// define handler aliases
const Renderer json = const _JsonRenderer();
const Renderer text = const _TextRenderer();

class _JsonRenderer implements Renderer {
    static final _logger = LoggerFactory.getLoggerFor(_JsonRenderer);

    const _JsonRenderer();

    @override
    ContentType get contentType => Mime.APPLICATION_JSON;

    @override
    Stream<List<int>> render(String template, Object model) {
        _logger.debug("render model = [$model]");
        var json = JSON.encode(model);
        var controller;
        var listen = () {
            controller.add(UTF8.encode(json));
            controller.close();
        };
        controller = new StreamController<List<int>>(onListen: listen);
        return controller.stream;
    }
}

class _TextRenderer implements Renderer {
    static final _logger = LoggerFactory.getLoggerFor(_TextRenderer);

    const _TextRenderer();

    @override
    ContentType get contentType => Mime.TEXT_HTML;

    @override
    Stream<List<int>> render(String template, dynamic model) {
        _logger.debug("render model = [$model]");
        var controller;
        String str = model != null ? model.toString() : null;
        var listen = () {
            controller.add(UTF8.encode(str));
            controller.close();
        };
        controller = new StreamController<List<int>>(onListen: listen);
        return controller.stream;
    }
}